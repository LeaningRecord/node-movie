//app.js的内容
// 连接mongodb（必须）
var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/test');


var routes = require('./routes');
var express = require('express');
var path = require('path');
var port=process.env.PORT || 3010;
var app = express();

//form提交表单，解析body的（必须）
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views/pages'));

app.set('view engine', 'jade');

//入口页
app.get('/', routes.first);
//电影列表页
app.get('/list', routes.index(db));
//电影详情页
app.get('/detail/:id', routes.detail(db));
//增加电影页
app.get('/admin/new', routes.new);
//修改电影页
app.get('/admin/update/:id', routes.update(db));
//后台列表页
app.get('/admin/list', routes.list(db));
//接口
app.post('/update_movie/:id',routes.update_movie(db));
app.post('/add_movie', routes.add_movie(db));
app.post('/delete_movie/:id', routes.delete_movie(db));

app.listen(port);
console.log('movie is running on port:'+port);

module.exports = app;

