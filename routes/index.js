//.routes/index.js
var moment = require("moment");

exports.first = function(req, res){
  res.render('index', { title: '入口地址' });
}

//首页
exports.index = function(db){
  return function(req, res) {
    var collection = db.get('movie');
    collection.find({},{},function(e,docs){

      res.render('movie_list', {
        "title":"电影列表",
        "data" : docs
      });
    });
  };
};
//列表页
exports.list = function(db) {
  return function(req, res) {
    var collection = db.get('movie');
    collection.find({},{},function(e,docs){
      res.render('admin_list', {
        "title":"后台列表",
        "data" : docs
      });
    });
  };
};

//新增电页影
exports.new = function(req, res){
  res.render('admin_new', { title: '添加电影' });
};

//电影详情
exports.detail = function(db){
  return function(req, res) {
    console.log("req.params.id:");
    console.log(req.params.id);
    var collection = db.get('movie');
    collection.find({"_id":req.params.id },{safe:true},function(e,docs){
      res.render('movie_detail', {
        "title":"电影详情",
        "data" : docs[0]
      });
    });
  };
};

//电影更新页
exports.update = function(db){
  return function(req, res) {
    var collection = db.get('movie');
    collection.findOne({"_id":req.params.id },{safe:true},function(err,docs){
      console.log(docs);
      if (err) {
        res.send("There was a problem adding the information to the database.");
      } else {
        res.render('admin_update', {
          "title":"更新电影",
          "movie" : docs
        });
      }
    });
  };
};

//接口（add）
exports.add_movie=function(db){

  return function(req, res) {

    var movie=req.body.movie;
    console.log(req.body.movie);

    var collection = db.get('movie');
    // Submit to the DB
    collection.insert({
      "doctor": movie.doctor,
      "title": movie.title,
      "language": movie.language,
      "country": movie.country,
      "summary": movie.summary,
      "flash": movie.flash,
      "poster": movie.poster,
      "year": movie.year,
      "meta":{
        "createAt": moment().format("YYYY-MM-DD HH:mm:ss"),
        "updateAt": moment().format("YYYY-MM-DD HH:mm:ss")
      }
    }, function (err, doc) {
      if (err) {
        // If it failed, return error
        res.send("There was a problem adding the information to the database.");
      }
      else {
        // And forward to success page
        res.redirect("/admin/list");
      }
    });
  }
}

//接口（update）
exports.update_movie=function(db){
  return function(req, res) {

    var movie=req.body.movie;
    console.log("即将保存的movie信息");
    console.log(req.params.id);
    // Set our collection
    var collection = db.get('movie');
    collection.update(
    {"_id":req.params.id}, {$set:
        {doctor: movie.doctor,
          title: movie.title,
          language: movie.language,
          country: movie.country,
          summary: movie.summary,
          flash: movie.flash,
          poster: movie.poster,
          year: movie.year,
          meta:{
            "createAt": movie.createAt,
            updateAt: moment().format("YYYY-MM-DD HH:mm:ss")
          }
        }}, {safe:true}, function (err, doc) {
          if (err) {
            res.send("There was a problem adding the information to the database.");
        } else {
             res.redirect("/admin/list");
      }
    });
  }
};

//接口（delete）
exports.delete_movie=function(db){
  return function(req, res) {
    var collection = db.get('movie');
    collection.remove({_id:req.params.id},{safe:true},function(err,docs){
      res.redirect("/admin/list");
    });
  };
}