var express = require('express');
var router = express.Router();

var data={
    title:"movie 列表",
    movies:[
        {"id":0,"title":"澳门风云","post":"/images/amfy.jpg"},
        {"id":1,"title":"泰坦尼克号","post":"/images/ttnkh.jpg"},
        {"id":2,"title":"喜剧之王","post":"/images/xjzw.jpg"}
    ]};

var detail={
    flash:"http://player.youku.com/player.php/sid/XNjA1Njc0NTUy/v.swf",
    title:"动物世界",
    doctor:"Jemmy",
    country:"美国",
    language:"英语",
    year:"2009-9-8",
    summary:"动物是人类的朋友，了解大自然的动物",
    meta:{createAt:"2010-2-1"}
}

var list={
    data:[
        {
            id:0,
            flash:"http://player.youku.com/player.php/sid/XNjA1Njc0NTUy/v.swf",
            title:"动物世界",
            doctor:"Jemmy",
            country:"美国",
            language:"英语",
            year:"2009-9-8",
            summary:"动物是人类的朋友，了解大自然的动物",
            meta:{createAt:"2010-2-1"}
        },{
            id:1,
            flash:"http://player.youku.com/player.php/sid/XNjA1Njc0NTUy/v.swf",
            title:"动物世界",
            doctor:"Jemmy",
            country:"美国",
            language:"英语",
            year:"2009-9-8",
            summary:"动物是人类的朋友，了解大自然的动物",
            meta:{createAt:"2010-2-1"}
        },{
             id:2,
            flash:"http://player.youku.com/player.php/sid/XNjA1Njc0NTUy/v.swf",
                title:"动物世界",
                doctor:"Jemmy",
                country:"美国",
                language:"英语",
                year:"2009-9-8",
                summary:"动物是人类的朋友，了解大自然的动物",
                meta:{createAt:"2010-2-1"}
        }

    ]

}


var admin={
    title:"后台录入页",
    data:{
        title:"",
        doctor:"",
        country:"",
        year:"",
        poster:"",
        flash:"",
        summary:"",
        language:""
    }

}
/* GET home page. */
router.get('/', function(req, res, next) {
    console.log("电影首页");
    res.render('index', {data: data.movies});
});


/* GET home page. */
router.get('/list', function(req, res, next) {

    res.render('list',{ data:list.data });
});


/* GET home page. */
router.get('/detail/:id', function(req, res, next) {
    var id=req.params.id;
    console.log(id);

    res.render('detail',{ data:detail });

});

/* GET home page. */
router.get('/admin/*', function(req, res, next) {
    var id=req.params.id;
    console.log(id);

    res.render('admin',{movie:admin.data});

});


module.exports = router;
